/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/13 17:27:35 by almoraru          #+#    #+#             */
/*   Updated: 2018/11/13 17:36:57 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_check_errors(char *base)
{
	int i;
	int j;

	i = 0;
	j = 0;
	if (!base)
		return (0);
	while (base[i] != '\0')
	{
		if (base[i] == '-' || base[i] == '+')
			return (0);
		j = i + 1;
		while (base[j] != '\0')
		{
			if (base[i] == base[j])
				return (0);
			j++;
		}
		i++;
	}
	if (i < 2)
		return (0);
	return (i);
}

static void		ft_print_base(int nbr, char *base, unsigned int lenght)
{
	unsigned int	nb;

	nb = nbr;
	if (nbr < 0)
	{
		ft_putchar('-');
		nb = -nbr;
	}
	if (nb >= lenght)
		ft_print_base(nb / lenght, base, lenght);
	ft_putchar(base[nb % lenght]);
}

void			ft_putnbr_base(int nbr, char *base)
{
	unsigned int lenght;

	lenght = ft_check_errors(base);
	if (lenght < 2)
		return ;
	ft_print_base(nbr, base, lenght);
}
