/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 18:36:50 by almoraru          #+#    #+#             */
/*   Updated: 2018/11/10 17:41:16 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char		tar;
	size_t		i;

	i = ft_strlen((char*)s);
	tar = (char)c;
	while ((int)i >= 0)
	{
		if (s[i] == tar)
			return (&((char*)s)[i]);
		i--;
	}
	return (NULL);
}
