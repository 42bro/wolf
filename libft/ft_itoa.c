/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/09 18:53:42 by almoraru          #+#    #+#             */
/*   Updated: 2018/11/10 19:42:00 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static		int		ft_zeros(int n)
{
	int i;
	int nb;

	i = 0;
	nb = n;
	if (nb == 0)
		return (1);
	while (nb)
	{
		nb /= 10;
		i++;
	}
	if (n < 0)
		return (i + 1);
	else
		return (i);
}

char				*ft_itoa(int n)
{
	char	*ret;
	int		nb;
	int		len;

	nb = n;
	len = ft_zeros(nb);
	ret = ft_strnew(len);
	if (ret == NULL)
		return (NULL);
	while (len >= 0)
	{
		len--;
		if (n < 0)
			ret[len] = (((nb % 10) * -1) + '0');
		else
			ret[len] = ((nb % 10) + '0');
		nb /= 10;
	}
	if (n < 0)
		ret[0] = '-';
	return (ret);
}
