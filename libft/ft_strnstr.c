/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 16:36:17 by almoraru          #+#    #+#             */
/*   Updated: 2018/11/10 19:18:39 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t			i;
	size_t			j;
	const char		*src;
	const char		*find;

	if (needle[0] == '\0')
		return ((char*)haystack);
	src = haystack;
	find = needle;
	i = 0;
	while (src[i] != '\0')
	{
		if (src[i] == find[0])
		{
			j = 1;
			while (find[j] != '\0' && src[i + j] == find[j])
				j++;
			if (find[j] == '\0' && (i + j) <= len)
				return (&((char*)src)[i]);
		}
		i++;
	}
	return (NULL);
}
