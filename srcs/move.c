/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/03 10:39:00 by fdubois           #+#    #+#             */
/*   Updated: 2019/06/18 09:56:36 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	move_right_or_left(t_env *e, double old_dir_x
							, double old_plane_x, int i)
{
	if (i == 1)
	{
		old_dir_x = e->cam.dir_x;
		e->cam.dir_x = e->cam.dir_x * cos(-0.05f) - e->cam.dir_y * sin(-0.05f);
		e->cam.dir_y = old_dir_x * sin(-0.05f) + e->cam.dir_y * cos(-0.05f);
		old_plane_x = e->plane.x;
		e->plane.x = e->plane.x * cos(-0.05f) - e->plane.y * sin(-0.05f);
		e->plane.y = old_plane_x * sin(-0.05f) + e->plane.y * cos(-0.05f);
	}
	else
	{
		old_dir_x = e->cam.dir_x;
		e->cam.dir_x = e->cam.dir_x * cos(0.05f) - e->cam.dir_y * sin(0.05f);
		e->cam.dir_y = old_dir_x * sin(0.05f) + e->cam.dir_y * cos(0.05f);
		old_plane_x = e->plane.x;
		e->plane.x = e->plane.x * cos(0.05f) - e->plane.y * sin(0.05f);
		e->plane.y = old_plane_x * sin(0.05f) + e->plane.y * cos(0.05f);
	}
}

void	move_forward_or_backward(t_env *e, int i)
{
	if (i == 1)
	{
		if (e->map[(int)(e->pos.x + e->cam.dir_x * 0.11f)][(int)(e->pos.y)]
			!= W)
			e->pos.x += e->cam.dir_x * 0.11f;
		if (e->map[(int)(e->pos.x)][(int)(e->pos.y + e->cam.dir_y)]
			!= W)
			e->pos.y += e->cam.dir_y * 0.11f;
	}
	else
	{
		if (e->map[(int)(e->pos.x - e->cam.dir_x * 0.11f)][(int)(e->pos.y)]
			!= W)
			e->pos.x -= e->cam.dir_x * 0.11f;
		if (e->map[(int)(e->pos.x)][(int)(e->pos.y - e->cam.dir_y * 1)]
			!= W)
			e->pos.y -= e->cam.dir_y * 0.11f;
	}
}
