/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/26 12:03:01 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/03 15:55:57 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	free_everything_and_quit(t_env *e)
{
	if (e->renderer != NULL)
		free_textures(e);
	if (e->screen_buf != NULL)
		destroy_texture(e->screen_buf, e);
	free_audio(e);
	free_sdl(e);
	ft_putendl("Done!");
	exit(69);
}

void	ft_error(char *s, t_env *e)
{
	ft_putstr("Error! ");
	ft_putstr(s);
	ft_putstr("Freeing...");
	free_everything_and_quit(e);
}

void	ft_error_parse(char *s, t_li *list, t_env *e)
{
	ft_putstr("Error! ");
	ft_putstr(s);
	ft_putstr("Freeing...");
	free_list(&list, e->maph);
	free_map(e->map, e->mapw, e->maph);
	ft_putendl("Done!");
	exit(69);
}

void	ft_error_line(char *s, t_env *e, char *line)
{
	ft_putstr("Error! ");
	ft_putstr(s);
	ft_putstr("Freeing...");
	ft_putendl("Done!");
	if (line != NULL)
		free(line);
	free_map(e->map, e->mapw, e->maph);
	exit(69);
}
