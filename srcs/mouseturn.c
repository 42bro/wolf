# define MAX_SPEED 1.0f
# define CH_HARDBASS 42
# define HARDBASS_RADIUS 50

void	mouse_turn(t_env *e, Sint32 x)
{
	int	old_dir_x;
	int	old_plane_x;
    double dir;

    dir = ((double)x) / (WIN_WIDTH / 2.0f);
	old_dir_x = e->cam.dir_x;
	e->cam.dir_x = e->cam.dir_x * cos(dir) - e->cam.dir_y * sin(dir);
	e->cam.dir_y = old_dir_x * sin(dir) + e->cam.dir_y * cos(dir);
	old_plane_x = e->plane.x;
	e->plane.x = e->plane.x * cos(dir) - e->plane.y * sin(dir);
	e->plane.y = old_plane_x * sin(dir) + e->plane.y * cos(dir);
}

if (e->event.type == SDL_MOUSEMOTION && e->event.motion.xrel)
    mouse_turn(e, e->event.motion.xrel);

void	new_mvmt(t_env *e)
{
	static double acc = 0;


	if (e->pos.x < e->mapw - 1 && e->pos.y < e->maph - 1 && e->pos.x > 1 && e->pos.y > 1)
	{
		if (e->mv & M_F)
			if (acc <= 0)
				acc = 1;
			else if (e->speed < MAX_SPEED)
				acc++;
		else if (e->mv & M_B)
			if (acc >= 0)
				acc = -1;
			else if (e->speed < MAX_SPEED)
				acc--;
		else if (acc && !(e->mv & (M_B | M_F)))
			(acc < 0 ? acc++ : acc--);
		e->speed = (1.0f - log10((acc >= 0 ? acc : -acc))) * MAX_SPEED;
		if (e->map[(int)(e->pos.x + e->cam.dir_x * e->speed)][(int)(e->pos.y)] != W)
				e->pos.x += e->cam.dir_x * e->speed;
		if (e->map[(int)(e->pos.x)][(int)(e->pos.y + e->cam.dir_y * e->speed)] != W)
				e->pos.y += e->cam.dir_y * e->speed;
	}
}

int		get_hardbass_sprite_nb(t_env *e)
{
	int i;

	i = e->sprites_nb;
	while (--i >= 0 && e->sprite_dist[i] < HARDBASS_RADIUS)
		if (e->s[sprite_order[i]].flags & S_RAD)
			return (i);
	return (-1);
}

Sint16	get_hardbass_angle(t_env *e, int i)
{
	t_pos v1;
	t_pos v2;
	double dot;
	double det;
	double ret;

	v1.x = e->cam.dir_x;
	v1.y = e->cam.dir_y;
	v2.x = e->s[sprite_order[i]].x - e->pos.x;
	v2.y = e->s[sprite_order[i]].y - e->pos.y;
	dot = v1.x * v2.x + v1.y * v2.y;
	det = v1.x * v2.y - v1.y * v2.x;
	ret = atan2(det, dot));
	return ((int)((ret / PI) * 180.0));
}


radio = get_hardbass_sprite_nb(e);
if (radio < 0 && Mix_Playing(CH_HARDBASS))
	Mix_HaltChannel(CH_HARDBASS);
else
{
	if (e->sprite_dist[radio] > 0.5f)
		Mix_SetPosition(CH_HARDBASS, get_hardbass_angle(e, i), (((double)(e->sprite_dist[radio] / HARDBASS_RADIUS)) * 255));
	if (!Mix_Playing(CH_HARDBASS))
		Mix_PlayChannel(CH_HARDBASS, e->sfx[SFX_RAD], -1);
}