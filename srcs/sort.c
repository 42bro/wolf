/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 18:09:58 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/01 22:02:52 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	sprite_swap2(double *a, double *b)
{
	double tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
}

void	sprite_swap(int *a, int *b)
{
	int tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
}

int		skip_turtles(int gap)
{
	gap = (gap * 10) / 13;
	if (gap == 9 || gap == 10)
		gap = 11;
	if (gap < 1)
		gap = 1;
	return (gap);
}

void	comb_sort(int *order, double *dist, int amount)
{
	int gap;
	int swap;
	int i;
	int j;

	gap = amount;
	swap = FALSE;
	while (gap > 1 || swap)
	{
		gap = skip_turtles(gap);
		swap = FALSE;
		i = -1;
		while (++i < amount - gap)
		{
			j = i + gap;
			if (dist[i] < dist[j])
			{
				sprite_swap2(&dist[i], &dist[j]);
				sprite_swap(&order[i], &order[j]);
				swap = TRUE;
			}
		}
	}
}
