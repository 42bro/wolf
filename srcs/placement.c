/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   placement.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/03 12:11:22 by fdubois           #+#    #+#             */
/*   Updated: 2019/06/13 11:35:51 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

int		put_sprite_tex(t_env *e, int i, int x, int y)
{
	if (e->map[y][x] == A)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/ak.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_AKM;
		i++;
	}
	else if (e->map[y][x] == B)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/pics/barrel.png")))
			ft_error("Failed to load texture!", e);
		i++;
	}
	else if (e->map[y][x] == C)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/pics/greenlight.png")))
			ft_error("Failed to load texture!", e);
		i++;
	}
	else if (e->map[y][x] == D)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/pics/pillar.png")))
			ft_error("Failed to load texture!", e);
		i++;
	}
	else if (e->map[y][x] == E)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/pics/eagle.png")))
			ft_error("Failed to load texture!", e);
		i++;
	}
	else if (e->map[y][x] == G)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/gopnik.png")))
			ft_error("Failed to load texture!", e);
		e->sprite_tex[i]->flags = S_PNJ;
		i++;

	}
	else if (e->map[y][x] == H)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/gopnik2.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_PNJ;
		i++;
	}
	else if (e->map[y][x] == I)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/gopnik5.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_PNJ;
		i++;
	}
	else if (e->map[y][x] == J)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/gopnikboss.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_PNJ;
		i++;
	}	
	else if (e->map[y][x] == V)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/vodka.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_VOD;
		i++;
	}
	else if (e->map[y][x] == X)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/vodka2.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_VOD;
		i++;
	}
	else if (e->map[y][x] == Y)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/vodka3.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_VOD;
		i++;
	}
	else if (e->map[y][x] == Z)
	{
		if (!(e->sprite_tex[i] = png2surf("textures/sprites/vodka4.png")))
			ft_error("Failed to load texture!", e);
		e->s[i].flags = S_VOD;
		i++;
	}
	return (i);
}

void	get_sprite_pos(t_env *e)
{
	int y;
	int x;
	int i;

	y = -1;
	i = 0;
	while (++y < e->maph)
	{
		x = -1;
		while (++x < e->mapw)
		{
			if (e->map[y][x] > W && i < e->sprite_nb)
			{
				e->s[i].x = y + 0.5f;
				e->s[i].y = x + 0.5f;
				i = put_sprite_tex(e, i, x, y);
			}
		}
	}
}

void	get_nb_sprites(t_env *e)
{
	int y;
	int x;

	y = -1;
	while (++y < e->maph)
	{
		x = -1;
		while (++x < e->mapw)
		{
			if (e->map[y][x] == P)
			{
				e->pos.x = (double)y + 0.5f;
				e->pos.y = (double)x + 0.5f;
			}
		}
	}
}
