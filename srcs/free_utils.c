/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/03 15:55:17 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/13 10:05:40 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	free_audio(t_env *e)
{
	int	i;

	i = -1;
	if (Mix_PlayingMusic())
		Mix_HaltMusic();
	while (++i < NB_BGM)
		Mix_FreeMusic(e->bgm[i]);
	while (++i < NB_SFX)
		Mix_FreeChunk(e->sfx[i]);
	Mix_CloseAudio();
}

void	free_sdl(t_env *e)
{
	if (e->renderer != NULL)
		SDL_DestroyRenderer(e->renderer);
	if (e->win != NULL)
		SDL_DestroyWindow(e->win);
	SDL_Quit();
}

void	free_textures(t_env *e)
{
	int i;

	i = -1;
	while (++i < NB_TEXTURES)
	{
		if (e->textures[i] != NULL)
			SDL_FreeSurface(e->textures[i]);
	}
	if (e->floor_tex[0] != NULL)
		SDL_FreeSurface(e->floor_tex[0]);
	if (e->floor_tex[1] != NULL)
		SDL_FreeSurface(e->floor_tex[1]);
	i = -1;
	while (++i < e->sprite_nb)
	{
		if (e->sprite_tex[i] != NULL)
			SDL_FreeSurface(e->sprite_tex[i]);
	}
}

void	free_map(signed char **map, int mapw, int maph)
{
	int y;
	int x;

	if (map != NULL)
	{
		y = -1;
		while (++y < maph)
		{
			x = -1;
			while (++x < mapw)
			{
				if (map[y][x])
					free(&map[y][x]);
			}
		}
		free(map);
	}
}
