#include "../includes/wolf3d.h"

#define I_VOD        1
#define I_AK        (1 << 1)

#define NB_CHANNELS 64

void    add_to_sprite_map_placement(t_env *e)
{
	if (case == A)
		e->sprite_tex[i]->flags = S_AKM;
	if (case == V)
		e->sprite_tex[i]->flags = S_VOD;
	if (ft_strchr("GHIJ", case))
		e->sprite_tex[i]->flags = S_PNJ;
}

void    set_background_volume(t_env *e)
{

}

void    inventory_update(t_env *e)
{
	if (!(e->inv & I_AK) && e->sprite_dist[e->sprite_order[0]] < 0.5f && e->s[sprite_order[0]]->flags & S_AKM))
	{ 
		e->inv |= I_AK;
		Mix_PlayChannel(-1, e->sfx[SFX_AKD], 0);
		Mix_PlayChannel(-1, e->sfx[SFX_AKE], 0);

		e->map[(int)e->s[sprite_order[0]]->y][(int)e->s[sprite_order[0]]->x] = F;
	}
	else if (e->sprite_dist[e->sprite_order[0]] < 0.5f && e->s[sprite_order[0]]->flags & S_VOD))
	{
		if (!(e->inv & I_VOD))
			e->inv |= I_VOD;    
		Mix_PlayChannel(-1, e->sfx[SFX_VOD], 0);
		e->map[(int)e->s[sprite_order[0]]->y][(int)e->s[sprite_order[0]]->x] = F;
	}
}

void    shoot(t_env *e)
{
	if (!(e->inv & I_AK))
		return ;
	if (!Mix_Playing(NB_CHANNELS - 1))
		Mix_PlayChannel(NB_CHANNELS - 1, e->sfk[SFX_AKS], 0);
}

void    init_ui_textures(t_env *e)
{

}

void    display_vodka(t_env *e)
{
	int i;

	i = -1;
	while (++i < e->vodka)
	{
		SDL_RenderCopy(e->renderer, t->texture, NULL, &(SDL_Rect){.x = (i + 1) * e->, .y = WIN_HEIGHT / 2, .w = WIN_WIDTH / 2, .h = WIN_HEIGHT / 2});
	}
}

void    render_hud(t_env *e)
{

	if (e->inv & I_AK)
		SDL_RenderCopy(e->renderer, t->texture, NULL, &(SDL_Rect){.x = WIN_WID
				TH / 2, .y = WIN_HEIGHT / 2, .w = WIN_WIDTH / 2, .h = WIN_HEIGHT / 2});
	if (e->inv & I_VOD)
		display_vodka(e);
}

int     get_angle(t_env *e, int sprite)
{
}

void    radio_sound(t_env *e, int sprite, double dist, int chan)
{
	if (dist && dist < 10)
		Mix_SetPosition(chan, get_angle(, ), 255 - ((10f / dist) * 255));
	else if (dist >= 10)
		Mix_SetPosition(chan, 0, 0);  
}
