/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/01 15:28:40 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/03 16:38:50 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	fill_tab(t_env *e, t_li *list)
{
	t_li	*tmp;
	int		i;
	int		j;

	tmp = list;
	i = -1;
	if (!list || !ft_stris(list->str, 'W'))
		ft_error_parse("Just no.", list, e);
	while (tmp)
	{
		e->map[++i] = tmp->str;
		j = -1;
		while (++j < e->mapw)
			get_map_value(&e->map[i][j]);
		if (!tmp->next && !ft_stris(e->map[i], W))
			ft_error_parse("last line sucks.", list, e);
		tmp = tmp->next;
	}
}

void	check_line(t_env *e, char *line)
{
	int	i;

	i = -1;
	while (line[++i])
	{
		if (line[i] == 'P')
		{
			if (e->pos.x || e->pos.y)
				ft_error_line("invalid map: several start points in map.", e
, line);
			else
			{
				e->pos.x = e->maph + 0.5;
				e->pos.y = i + 0.5;
			}
		}
		else if (!ft_strchr(MAP_CHARS_LABEL, line[i]))
			ft_error_line("invalid map: bad characters.", e, line);
		else if (ft_strchr(MAP_SPRITES_LABEL, line[i]))
			if (++e->sprite_nb >= NB_SPRITES)
				ft_error_line("invalid map: too many sprites.", e, line);
	}
}

void	parse(t_env *e, int ac, char **av)
{
	static int	fd = 0;
	char		*line;
	t_li		*list;

	list = NULL;
	if (ac != 2 || !(av[1][0]) || ((fd = open(av[1], O_RDONLY)) < 0))
		ft_error_parse("usage: ./wolf3d [map file]", list, e);
	while (get_next_line(fd, &line) == 1)
	{
		check_line(e, line);
		if (!e->maph)
			e->mapw = ft_strlen(line);
		if (e->maph && ((size_t)e->mapw != ft_strlen(line)))
			ft_error_parse("invalid map: lines of different length", list, e);
		if (line[0] != 'W' || line[e->mapw - 1] != 'W')
			ft_error_parse("invalid map: lacks some walls", list, e);
		add_node(&list, new_node((signed char*)line));
		e->maph++;
	}
	if (!(e->map = (signed char**)malloc(sizeof(signed char*) * e->maph)))
		ft_error_parse("malloc sucks ! couldn't allocate the map", list, e);
	fill_tab(e, list);
	free_list(&list, e->maph);
	close(fd);
}
