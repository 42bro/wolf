/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl_events.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 11:41:11 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/03 16:20:17 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

# define MAX_SPEED 0.1f

void	new_mvmt(t_env *e)
{
	static double acc = 0;

	if (e->pos.x < e->mapw - 1 && e->pos.y < e->maph - 1 && e->pos.x > 1 && e->pos.y > 1)
	{
		if (e->mv & M_F)
		{
			if (acc <= 0)
				acc = 1;
			else if (e->speed < MAX_SPEED)
				acc++;
		}
		else if (e->mv & M_B)
		{
			if (acc >= 0)
				acc = -1;
			else if (e->speed < MAX_SPEED)
				acc--;
		}
		else if (!(e->mv & (M_B | M_F)))
		{
			if (acc)
				(acc < 0 ? acc++ : acc--);
			else
				e->speed = 0;
		}
		if (!acc)
			e->speed = (e->speed ? e->speed / 2 : 0);
		else
			e->speed = log10((acc > 0 ? acc : -acc)) * MAX_SPEED * ((e->mv & M_B ? -1 : 1));
		printf("e->speed %lf acc %lf \n", e->speed, acc);
		if (e->map[(int)(e->pos.x + e->cam.dir_x * e->speed)][(int)(e->pos.y)] != W)
				e->pos.x += e->cam.dir_x * e->speed;
		if (e->map[(int)(e->pos.x)][(int)(e->pos.y + e->cam.dir_y * e->speed)] != W)
				e->pos.y += e->cam.dir_y * e->speed;
	}
}

int		is_mvmt_key(int key)
{
	return (key == SDLK_w || key == SDLK_s || key == SDLK_a
			|| key == SDLK_d || key == SDLK_q || key == SDLK_e);
}

void	move_it(t_env *e)
{
	if (!e->mv)
		Mix_FadeOutChannel(NB_CHANNELS - 1, 42);
	new_mvmt(e);
	if (e->mv & M_L)
		move_right_or_left(e, 0, 0, 2);
	if (e->mv & M_R)
		move_right_or_left(e, 0, 0, 1);
}

void	wasd(t_env *e, char mode)
{
	if (e->event.key.keysym.sym == SDLK_w)
		e->mv = (mode ? e->mv | M_F : e->mv & ~M_F);
	else if (e->event.key.keysym.sym == SDLK_s)
		e->mv = (mode ? e->mv | M_B : e->mv & ~M_B);
	else if (e->event.key.keysym.sym == SDLK_a)
		e->mv = (mode ? e->mv | M_L : e->mv & ~M_L);
	else if (e->event.key.keysym.sym == SDLK_d)
		e->mv = (mode ? e->mv | M_R : e->mv & ~M_R);
	else if (e->event.key.keysym.sym == SDLK_q)
		e->mv = (mode ? e->mv | M_SL : e->mv & ~M_SL);
	else if (e->event.key.keysym.sym == SDLK_e)
		e->mv = (mode ? e->mv | M_SR : e->mv & ~M_SR);
}
/*
void	shoot(t_env *e)
{

}*/

void	mouse_turn(t_env *e, double old_dir_x, double old_plane_x, Sint32 x)
{
    double dir;

    dir = ((double)x) / (WIN_WIDTH / 2.0f);
	old_dir_x = e->cam.dir_x;
	e->cam.dir_x = e->cam.dir_x * cos(dir) - e->cam.dir_y * sin(dir);
	e->cam.dir_y = old_dir_x * sin(dir) + e->cam.dir_y * cos(dir);
	old_plane_x = e->plane.x;
	e->plane.x = e->plane.x * cos(dir) - e->plane.y * sin(dir);
	e->plane.y = old_plane_x * sin(dir) + e->plane.y * cos(dir);
}

void	handle_sdl_events(t_env *e)
{
	double	old_dir_x;
	double	old_plane_x;

	old_dir_x = 0.0f;
	old_plane_x = 0.0f;
	while ((SDL_PollEvent(&e->event) != 0))
	{
		if (e->event.type == SDL_MOUSEMOTION)
			mouse_turn(e, old_dir_x, old_plane_x, -e->event.motion.xrel);
		else if (e->event.type == SDL_KEYUP
			&& is_mvmt_key(e->event.key.keysym.sym))
			wasd(e, 0);
		else if (e->event.type == SDL_KEYDOWN)
		{
			if (is_mvmt_key(e->event.key.keysym.sym))
			{
				if (!Mix_Playing(NB_CHANNELS - 1))
					Mix_PlayChannel(NB_CHANNELS - 1, e->sfx[0], 0);
				wasd(e, 1);
			}
			else if (e->event.key.keysym.sym == SDLK_ESCAPE)
				free_everything_and_quit(e);
		}
		else if (e->event.type == SDL_MOUSEBUTTONDOWN)
		{
			if (e->event.key.keysym.sym == SDL_BUTTON_LEFT)
			{
				Mix_PlayChannel(-1, e->sfx[10], 0);
				if (e->hitscam != e->screen_buf[(WIN_HEIGHT / 2) * WIN_WIDTH + (WIN_WIDTH / 2)])
					Mix_PlayChannel(-1, e->sfx[4], 0);
			}
		}
		else if (e->event.type == SDL_QUIT)
			free_everything_and_quit(e);
		move_it(e);
	}
}
