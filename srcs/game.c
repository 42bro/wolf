/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 17:42:57 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/03 15:55:43 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

void    inventory_update(t_env *e)
{
	if (!(e->inv & I_AKM) && (int)e->s[e->sprite_order[e->sprite_nb - 1]].x == (int)e->pos.x 
	&& (int)e->s[e->sprite_order[e->sprite_nb - 1]].y == (int)e->pos.y && e->s[e->sprite_order[e->sprite_nb - 1]].flags & S_AKM)
	{ 
		e->inv |= I_AKM;
		Mix_PlayChannel(NB_CHANNELS - 2, e->sfx[1], 0);
		Mix_PlayChannel(NB_CHANNELS - 2, e->sfx[2], 0);
		e->s[e->sprite_order[e->sprite_nb - 1]].y = 0;
		e->s[e->sprite_order[e->sprite_nb - 1]].x = 0;
	}
	else if ((int)e->s[e->sprite_order[e->sprite_nb - 1]].x == (int)e->pos.x 
	&& (int)e->s[e->sprite_order[e->sprite_nb - 1]].y == (int)e->pos.y && e->s[e->sprite_order[e->sprite_nb - 1]].flags & S_VOD)
	{
		if (!(e->inv & I_VOD))
			e->inv |= I_VOD;
		//if (Mix_Playing(NB_CHANNELS - 2))
			Mix_PlayChannel(NB_CHANNELS - 2, e->sfx[3], 0);
		++e->vodka;
		e->s[e->sprite_order[e->sprite_nb - 1]].y = 0;
		e->s[e->sprite_order[e->sprite_nb - 1]].x = 0;
		e->map[(int)e->pos.y][(int)e->pos.x] = F;
	}
}

void	run_game(t_env *e)
{
	e->g.game_ticks = 0;
	//if (Mix_PlayMusic(e->bgm[0], -1) == -1)
	//	ft_putendl(Mix_GetError());
	while (e->g.game)
	{
		e->g.time = SDL_GetTicks();
		handle_sdl_events(e);
		inventory_update(e);
		ray_cast(e);
		SDL_Delay(1);
		if (!(e->g.game_ticks++ % 10))
		{
			ft_putstr("FPS = ");
			ft_putnbr((int)(1000.0f / (float)(SDL_GetTicks() - e->g.time)));
			ft_putchar('\n');
		}
	}
}
