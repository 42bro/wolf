/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_caster.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: almoraru <almoraru@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 11:58:31 by almoraru          #+#    #+#             */
/*   Updated: 2019/06/13 11:32:33 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	calculate_initial_side_dist(t_env *e)
{
	if (e->ray.dir_x < 0)
	{
		e->ray.step_x = -1;
		e->ray.side_dist_x = (e->pos.x - e->m.x) * e->ray.delta_dist_x;
	}
	else
	{
		e->ray.step_x = 1;
		e->ray.side_dist_x = (e->m.x + 1.0f - e->pos.x) * e->ray.delta_dist_x;
	}
	if (e->ray.dir_y < 0)
	{
		e->ray.step_y = -1;
		e->ray.side_dist_y = (e->pos.y - e->m.y) * e->ray.delta_dist_y;
	}
	else
	{
		e->ray.step_y = 1;
		e->ray.side_dist_y = (e->m.y + 1.0f - e->pos.y) * e->ray.delta_dist_y;
	}
}

void	extend_ray_until_hit(t_env *e)
{
	while (e->ray.hit == 0)
	{
		if (e->ray.side_dist_x < e->ray.side_dist_y)
		{
			e->ray.side_dist_x += e->ray.delta_dist_x;
			e->m.x += e->ray.step_x;
			e->wall.side = 0;
		}
		else
		{
			e->ray.side_dist_y += e->ray.delta_dist_y;
			e->m.y += e->ray.step_y;
			e->wall.side = 1;
		}
		if (e->map[e->m.x][e->m.y] == W)
			e->ray.hit = TRUE;
	}
}

void	ray_cast(t_env *e)
{
	int x;

	x = -1;
	while (++x < WIN_WIDTH)
	{
		e->cam.x = 2 * x / (double)(WIN_WIDTH) - 1;
		e->ray.dir_x = e->cam.dir_x + e->plane.x * e->cam.x;
		e->ray.dir_y = e->cam.dir_y + e->plane.y * e->cam.x;
		e->m.x = (int)(e->pos.x);
		e->m.y = (int)(e->pos.y);
		e->ray.hit = 0;
		e->ray.delta_dist_x = fabs(1.0f / e->ray.dir_x);
		e->ray.delta_dist_y = fabs(1.0f / e->ray.dir_y);
		calculate_initial_side_dist(e);
		extend_ray_until_hit(e);
		prepare_draw(e);
		draw_walls(e, x);
		prepare_floor_casting(e);
		draw_floor(e, x);
	}
	e->hitscam = e->screen_buf[(WIN_HEIGHT / 2) * WIN_WIDTH + (WIN_WIDTH / 2)];
	sort_and_prep_sprites(e);
	SDL_RenderClear(e->renderer);
	slav_draw_full_screen_texture(e, e->screen_buf);
}
